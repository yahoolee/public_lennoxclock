/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.deskclock;

import android.app.ActionBar;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.lennox.deskclock.utils.ThemeUtils;
import com.lennox.deskclock.worldclock.Cities;
import com.lennox.deskclock.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

/**
 * Settings for the Alarm Clock.
 */
public class SettingsActivity extends PreferenceActivity
        implements Preference.OnPreferenceChangeListener {

    private static final int ALARMSCREEN_BACKGROUND = 1024;

    public static final String ALARMSCREEN_CUSTOM_BACKGROUND = "alarmscreen_background";

    private static final int ALARM_STREAM_TYPE_BIT =
            1 << AudioManager.STREAM_ALARM;

    static final String KEY_ALARM_IN_SILENT_MODE =
            "alarm_in_silent_mode";
    static final String KEY_SHOW_STATUS_BAR_ICON =
            "show_status_bar_icon";
    static final String KEY_ALARM_SNOOZE =
            "snooze_duration";
    static final String KEY_FLIP_ACTION =
            "flip_action";
    static final String KEY_SHAKE_ACTION =
            "shake_action";
    static final String KEY_VOLUME_BEHAVIOR =
            "volume_button_setting";
    static final String KEY_AUTO_SILENCE =
            "auto_silence";
    public static final String KEY_CLOCK_STYLE =
            "clock_style";
    public static final String KEY_HOME_TZ =
            "home_time_zone";
    public static final String KEY_AUTO_HOME_CLOCK =
            "automatic_home_clock";
    static final String KEY_VOLUME_BUTTONS =
            "volume_button_setting";
    static final String KEY_UNLOCK_ON_DISMISS =
            "unlock_on_dismiss";
    static final String KEY_VERSION =
            "preferences_application_version";
    public static final String KEY_DIGITAL_WIDGET_SHOW_AMPM =
            "digital_widget_show_ampm";

    public static final String DEFAULT_VOLUME_BEHAVIOR = "0";

    private static CharSequence[][] mTimezones;
    private long mTime;

    private ListPreference mCustomAlarmscreenBackground;
    private Preference mVersionPref;

    private File wallpaperImage;

    private File wallpaperTemporary;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        getWindow().setBackgroundDrawable(ThemeUtils.getDrawable(this, "window_background"));

        // We don't want to reconstruct the timezone list every single time
        // onResume() is called so we do it once in onCreate
        ListPreference listPref;
        listPref = (ListPreference) findPreference(KEY_HOME_TZ);
        if (mTimezones == null) {
            mTime = System.currentTimeMillis();
            mTimezones = getAllTimezones();
        }

        listPref.setEntryValues(mTimezones[0]);
        listPref.setEntries(mTimezones[1]);
        listPref.setSummary(listPref.getEntry());
        listPref.setOnPreferenceChangeListener(this);

        mCustomAlarmscreenBackground = (ListPreference) findPreference(ALARMSCREEN_CUSTOM_BACKGROUND);
        mCustomAlarmscreenBackground.setOnPreferenceChangeListener(this);
        mVersionPref = findPreference(KEY_VERSION);
        wallpaperImage = new File(getFilesDir() + "/alarmwallpaper");
        wallpaperTemporary = new File(getFilesDir() + "/alarmwallpaper.tmp");
        updateSummary();
    }

    @Override
    protected void onResume() {
        updateSummary();
        refresh();
        super.onResume();
    }

    private void updateSummary() {
        int resId;
        boolean hasCustom = false;
        File lockpaper = new File(getFilesDir() + "/alarmwallpaper");
        if ( lockpaper.exists() ) {
        hasCustom=true;
        }
        if (hasCustom) {
            resId = R.string.alarmscreen_custom_background_summary_image;
            mCustomAlarmscreenBackground.setValueIndex(mCustomAlarmscreenBackground.findIndexOfValue("0"));
        } else {
            resId = R.string.alarmscreen_custom_background_summary_default;
            mCustomAlarmscreenBackground.setValueIndex(mCustomAlarmscreenBackground.findIndexOfValue("1"));
        }
        mCustomAlarmscreenBackground.setSummary(getResources().getString(resId));

        boolean remove = true;
        try {
            getPackageManager().getPackageInfo("com.lennox.hub", PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            remove = false;
        }
        if (remove) {
            PreferenceGroup preferenceGroup = (PreferenceGroup) findPreference("preferences_application_category");
            if (preferenceGroup != null) getPreferenceScreen().removePreference(preferenceGroup);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ALARMSCREEN_BACKGROUND) {
            if (resultCode == RESULT_OK) {
                if (wallpaperTemporary.exists()) {
                    wallpaperTemporary.renameTo(wallpaperImage);
                }
                wallpaperImage.setReadOnly();
                Toast.makeText(this, getResources().getString(R.string.
                        alarmscreen_background_result_successful), Toast.LENGTH_LONG).show();
                mCustomAlarmscreenBackground.setValueIndex(0);
                updateSummary();
            } else {
                if (wallpaperTemporary.exists()) {
                    wallpaperTemporary.delete();
                }
                Toast.makeText(this, getResources().getString(R.string.
                        alarmscreen_background_result_not_successful), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
        if (KEY_ALARM_IN_SILENT_MODE.equals(preference.getKey())) {
            CheckBoxPreference pref = (CheckBoxPreference) preference;
            int ringerModeStreamTypes = Settings.System.getInt(
                    getContentResolver(),
                    Settings.System.MODE_RINGER_STREAMS_AFFECTED, 0);

            if (pref.isChecked()) {
                ringerModeStreamTypes &= ~ALARM_STREAM_TYPE_BIT;
            } else {
                ringerModeStreamTypes |= ALARM_STREAM_TYPE_BIT;
            }

            Settings.System.putInt(getContentResolver(),
                    Settings.System.MODE_RINGER_STREAMS_AFFECTED,
                    ringerModeStreamTypes);

            return true;
        } else if (KEY_VERSION.equals(preference.getKey())) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=lennykano%40gmail%2ecom&lc=AU&item_name=Lennox%20Corporation&item_number=LENNOX_LAUNCHER&currency_code=AUD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted"));
            startActivity(browserIntent);
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference pref, Object newValue) {
        if (KEY_AUTO_SILENCE.equals(pref.getKey())) {
            final ListPreference listPref = (ListPreference) pref;
            String delay = (String) newValue;
            updateAutoSnoozeSummary(listPref, delay);
        } else if (KEY_CLOCK_STYLE.equals(pref.getKey())) {
            final ListPreference listPref = (ListPreference) pref;
            final int idx = listPref.findIndexOfValue((String) newValue);
            ThemeUtils.setThemePackageName(this, (String) newValue);
            listPref.setSummary(listPref.getEntries()[idx]);
        } else if (KEY_HOME_TZ.equals(pref.getKey())) {
            final ListPreference listPref = (ListPreference) pref;
            final int idx = listPref.findIndexOfValue((String) newValue);
            listPref.setSummary(listPref.getEntries()[idx]);
            notifyHomeTimeZoneChanged();
        } else if (KEY_AUTO_HOME_CLOCK.equals(pref.getKey())) {
            boolean state =((CheckBoxPreference) pref).isChecked();
            Preference homeTimeZone = findPreference(KEY_HOME_TZ);
            homeTimeZone.setEnabled(!state);
            notifyHomeTimeZoneChanged();
        } else if (KEY_VOLUME_BUTTONS.equals(pref.getKey())) {
            final ListPreference listPref = (ListPreference) pref;
            final int idx = listPref.findIndexOfValue((String) newValue);
            listPref.setSummary(listPref.getEntries()[idx]);
        } else if (KEY_FLIP_ACTION.equals(pref.getKey())) {
            final ListPreference listPref = (ListPreference) pref;
            String action = (String) newValue;
            updateFlipActionSummary(listPref, action);
        } else if (KEY_SHAKE_ACTION.equals(pref.getKey())) {
            final ListPreference listPref = (ListPreference) pref;
            String action = (String) newValue;
            updateShakeActionSummary(listPref, action);
        } else if (KEY_SHOW_STATUS_BAR_ICON.equals(pref.getKey())) {
            // Check if any alarms are active. If yes and
            // we allow showing the alarm icon, the icon will be shown.
            Alarms.updateStatusBarIcon(getApplicationContext(), (Boolean) newValue);
        } else if (ALARMSCREEN_CUSTOM_BACKGROUND.equals(pref.getKey())) {
            int indexOf = mCustomAlarmscreenBackground.findIndexOfValue(newValue.toString());
            switch (indexOf) {
            case 0:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
                intent.setType("image/*");
                intent.putExtra("crop", "true");
                intent.putExtra("scale", true);
                intent.putExtra("scaleUpIfNeeded", false);
                intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
                int width = getWindowManager().getDefaultDisplay().getWidth();
                int height = getWindowManager().getDefaultDisplay().getHeight();
                Rect rect = new Rect();
                Window window = getWindow();
                window.getDecorView().getWindowVisibleDisplayFrame(rect);
                int statusBarHeight = rect.top;
                int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
                int titleBarHeight = contentViewTop - statusBarHeight;
                boolean isPortrait = getResources().getConfiguration().orientation ==
                    Configuration.ORIENTATION_PORTRAIT;
                intent.putExtra("aspectX", isPortrait ? width : height - titleBarHeight);
                intent.putExtra("aspectY", isPortrait ? height - titleBarHeight : width);
                try {
                    wallpaperTemporary.createNewFile();
                    wallpaperTemporary.setWritable(true, false);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(wallpaperTemporary));
                    intent.putExtra("return-data", false);
                    startActivityForResult(intent,ALARMSCREEN_BACKGROUND);
                } catch (IOException e) {
                } catch (ActivityNotFoundException e) {
                }
                updateSummary();
                return false;
            case 1:
                if (wallpaperImage.exists()) {
                    wallpaperImage.delete();
                }
                if (wallpaperTemporary.exists()) {
                    wallpaperTemporary.delete();
                }
                updateSummary();
                break;
            }
        }

        return true;
    }

    private void updateAutoSnoozeSummary(ListPreference listPref,
            String delay) {
        int i = Integer.parseInt(delay);
        if (i == -1) {
            listPref.setSummary(R.string.auto_silence_never);
        } else {
            listPref.setSummary(getString(R.string.auto_silence_summary, i));
        }
    }

    private void notifyHomeTimeZoneChanged() {
        Intent i = new Intent(Cities.WORLDCLOCK_UPDATE_INTENT);
        sendBroadcast(i);
    }

    private void updateFlipActionSummary(ListPreference listPref, String action) {
        int i = Integer.parseInt(action);
        listPref.setSummary(getString(R.string.flip_action_summary,
            getResources().getStringArray(R.array.flip_action_entries)[i]
                .toLowerCase()));
    }

    private void updateShakeActionSummary(ListPreference listPref, String action) {
        int i = Integer.parseInt(action);
        listPref.setSummary(getString(R.string.shake_summary, getResources()
            .getStringArray(R.array.flip_action_entries)[i].toLowerCase()));
    }

    private void refresh() {
        ListPreference listPref = (ListPreference) findPreference(KEY_AUTO_SILENCE);
        String delay = listPref.getValue();
        updateAutoSnoozeSummary(listPref, delay);
        listPref.setOnPreferenceChangeListener(this);

        listPref = (ListPreference)findPreference(KEY_CLOCK_STYLE);
        listPref.setOnPreferenceChangeListener(this);
        String[] packageList = ThemeUtils.createThemePackageList(this);
        listPref.setEntryValues(packageList);
        listPref.setEntries(ThemeUtils.createThemeNameList(this, packageList));
        String currentPackage = ThemeUtils.getThemePackageName(this, ThemeUtils.DEFAULT); 
        String currentThemeName = ThemeUtils.getThemeName(this, currentPackage); 
        listPref.setSummary(currentThemeName);

        Preference pref = findPreference(KEY_AUTO_HOME_CLOCK);
        boolean state =((CheckBoxPreference) pref).isChecked();
        pref.setOnPreferenceChangeListener(this);

        listPref = (ListPreference)findPreference(KEY_HOME_TZ);
        listPref.setEnabled(state);
        listPref.setSummary(listPref.getEntry());

        listPref = (ListPreference) findPreference(KEY_VOLUME_BUTTONS);
        listPref.setSummary(listPref.getEntry());
        listPref.setOnPreferenceChangeListener(this);

        CheckBoxPreference unlockOnDismiss = (CheckBoxPreference) findPreference(KEY_UNLOCK_ON_DISMISS);
        unlockOnDismiss.setOnPreferenceChangeListener(this);
        listPref = (ListPreference) findPreference(KEY_FLIP_ACTION);
        String action = listPref.getValue();
        updateFlipActionSummary(listPref, action);
        listPref.setOnPreferenceChangeListener(this);

        listPref = (ListPreference) findPreference(KEY_SHAKE_ACTION);
        String shake = listPref.getValue();
        updateShakeActionSummary(listPref, shake);
        listPref.setOnPreferenceChangeListener(this);

        CheckBoxPreference hideStatusbarIcon = (CheckBoxPreference) findPreference(KEY_SHOW_STATUS_BAR_ICON);
        hideStatusbarIcon.setOnPreferenceChangeListener(this);

        mVersionPref.setTitle(getString(R.string.app_label) + " " + getString(R.string.application_version));

        SnoozeLengthDialog snoozePref = (SnoozeLengthDialog) findPreference(KEY_ALARM_SNOOZE);
        snoozePref.setSummary();
    }

    private class TimeZoneRow implements Comparable<TimeZoneRow> {
        public final String mId;
        public final String mDisplayName;
        public final int mOffset;

        public TimeZoneRow(String id, String name) {
            mId = id;
            TimeZone tz = TimeZone.getTimeZone(id);
            boolean useDaylightTime = tz.useDaylightTime();
            mOffset = tz.getOffset(mTime);
            mDisplayName = buildGmtDisplayName(id, name, useDaylightTime);
        }

        @Override
        public int compareTo(TimeZoneRow another) {
            return mOffset - another.mOffset;
        }

        public String buildGmtDisplayName(String id, String displayName, boolean useDaylightTime) {
            int p = Math.abs(mOffset);
            StringBuilder name = new StringBuilder("(GMT");
            name.append(mOffset < 0 ? '-' : '+');

            name.append(p / DateUtils.HOUR_IN_MILLIS);
            name.append(':');

            int min = p / 60000;
            min %= 60;

            if (min < 10) {
                name.append('0');
            }
            name.append(min);
            name.append(") ");
            name.append(displayName);
            return name.toString();
        }
    }


    /**
     * Returns an array of ids/time zones. This returns a double indexed array
     * of ids and time zones for Calendar. It is an inefficient method and
     * shouldn't be called often, but can be used for one time generation of
     * this list.
     *
     * @return double array of tz ids and tz names
     */
    public CharSequence[][] getAllTimezones() {
        Resources resources = this.getResources();
        String[] ids = resources.getStringArray(R.array.timezone_values);
        String[] labels = resources.getStringArray(R.array.timezone_labels);
        if (ids.length != labels.length) {
            Log.wtf("Timezone ids and labels have different length!");
        }
        List<TimeZoneRow> timezones = new ArrayList<TimeZoneRow>();
        for (int i = 0; i < ids.length; i++) {
            timezones.add(new TimeZoneRow(ids[i], labels[i]));
        }
        Collections.sort(timezones);

        CharSequence[][] timeZones = new CharSequence[2][timezones.size()];
        int i = 0;
        for (TimeZoneRow row : timezones) {
            timeZones[0][i] = row.mId;
            timeZones[1][i++] = row.mDisplayName;
        }
        return timeZones;
    }

    public static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}

/**
 * 
 */

package com.lennox.deskclock.utils;

import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

/**
 * @author Andrew Neal TODO - clean this up
 */
public class ThemeUtils {

    // SharedPreferences
    public final static String DEFAULT = "Default";
    public final static String THEME_PREFERENCES = "theme_preferences";
    public final static String THEME_PACKAGE_NAME = "theme_package_name";

    public static String[] createThemeNameList(Context context, String[] packageNames) {
        String[] entries = new String[packageNames.length];
        for (int i = 0; i < packageNames.length; i++) {
            String themeName = ThemeUtils.getThemeName(context, packageNames[i]);
            entries[i] = themeName;
        }
        entries[0] = DEFAULT;
        return entries;
    }

    public static String[] createThemePackageList(Context context) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory("com.lennox.clock.THEME");
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> themes = pm.queryIntentActivities(intent, 0);
        String[] values = new String[themes.size() + 1];
        values[0] = context.getPackageName();
        for (int i = 0; i < themes.size(); i++) {
            String appPackageName = (themes.get(i)).activityInfo.packageName.toString();
            values[i + 1] = appPackageName;
        }
        values[0] = context.getPackageName();
        return values;
    }

    /**
     * @param mContext
     * @param view
     * @param resourceName
     */
    public static Drawable getDrawable(Context mContext, String resourceName) {
        String packageName = mContext.getPackageName();
        String themePackage = getThemePackageName(mContext, packageName);
        PackageManager pm = mContext.getPackageManager();
        Resources themeResources = null;
        Resources appResources = mContext.getResources();
        try {
            themeResources = pm.getResourcesForApplication(themePackage);
        } catch (NameNotFoundException e) {
            setThemePackageName(mContext, packageName);
            themeResources = appResources;
        }
        if (themeResources != null) {
            int resourceID = themeResources.getIdentifier(resourceName, "drawable", themePackage);
            if (resourceID != 0) {
               return themeResources.getDrawable(resourceID);
            }
            resourceID = appResources.getIdentifier(resourceName, "drawable", packageName);
            if (resourceID != 0) {
               return appResources.getDrawable(resourceID);
            }
        }
        return null;
    }

    /**
     * @param mContext
     * @param view
     * @param resourceName
     */
    public static int getColor(Context mContext, String resourceName) {
        String packageName = mContext.getPackageName();
        String themePackage = getThemePackageName(mContext, packageName);
        PackageManager pm = mContext.getPackageManager();
        Resources themeResources = null;
        Resources appResources = mContext.getResources();
        try {
            themeResources = pm.getResourcesForApplication(themePackage);
        } catch (NameNotFoundException e) {
            setThemePackageName(mContext, packageName);
            themeResources = appResources;
        }
        if (themeResources != null) {
            int resourceID = themeResources.getIdentifier(resourceName, "color", themePackage);
            if (resourceID != 0) {
               return themeResources.getColor(resourceID);
            }
            resourceID = appResources.getIdentifier(resourceName, "color", packageName);
            if (resourceID != 0) {
               return appResources.getColor(resourceID);
            }
        }
        return 0;
    }

    /**
     * @param mContext
     * @param packageName
     */
    public static String getThemeName(Context mContext, String packageName) {
        PackageManager pm = mContext.getPackageManager();
        String thisPackageName = mContext.getPackageName();
        Resources themeResources = null;
        if (!packageName.equals(thisPackageName)) {
            try {
                themeResources = pm.getResourcesForApplication(packageName);
            } catch (NameNotFoundException e) {
            }
        } else {
            return DEFAULT;
        }
        if (themeResources != null) {
            int resourceID = themeResources.getIdentifier("theme_name", "string", packageName);
            if (resourceID != 0) {
               return themeResources.getString(resourceID);
            }
        }
        return null;
    }

    /**
     * @param context
     * @param default_theme
     * @return theme package name
     */
    public static String getThemePackageName(Context context, String default_theme) {
        SharedPreferences sp = context.getSharedPreferences(THEME_PREFERENCES, 0);
        return sp.getString(THEME_PACKAGE_NAME, default_theme);
    }

    /**
     * @param context
     * @param packageName
     */
    public static void setThemePackageName(Context context, String packageName) {
        SharedPreferences sp = context.getSharedPreferences(THEME_PREFERENCES, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(THEME_PACKAGE_NAME, packageName);
        editor.commit();
    }

}

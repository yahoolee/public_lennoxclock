/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.deskclock;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lennox.deskclock.worldclock.WorldClockAdapter;
import com.lennox.deskclock.worldclock.CitiesActivity;

/**
 * Fragment that shows  the clock (analog or digital), the next alarm info and the world clock.
 */

public class ClockFragment extends DeskClockFragment implements OnSharedPreferenceChangeListener {

    private View mDigitalClock, mAnalogClock;
    private WorldClockAdapter mAdapter;
    private ListView mList;
    private SharedPreferences mPrefs;
    public Activity fa;
    private String mDefaultClockStyle;
    private String mClockStyle;

    private PendingIntent mQuarterlyIntent;
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
            @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            boolean changed = action.equals(Intent.ACTION_TIME_CHANGED)
                    || action.equals(Intent.ACTION_TIMEZONE_CHANGED)
                    || action.equals(Intent.ACTION_LOCALE_CHANGED);
            if (changed || action.equals(Utils.ACTION_ON_QUARTER_HOUR)) {
                if (mAdapter != null) {
                    // *CHANGED may modify the need for showing the Home City
                    if (changed && (mAdapter.hasHomeCity() != mAdapter.needHomeCity())) {
                        mAdapter.reloadData(context);
                    } else {
                        mAdapter.notifyDataSetChanged();
                    }
                    // Reloading the cities list with new localized names
                    if (action.equals(Intent.ACTION_LOCALE_CHANGED)) {
                        mAdapter.loadCitiesDb(context);
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
            if (changed) {
                mQuarterlyIntent = Utils.refreshAlarmOnQuarterHour(fa, mQuarterlyIntent);
            }
        }
    };

    private final Handler mHandler = new Handler();

    public ClockFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle icicle) {
        fa = super.getActivity();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(fa);
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.clock_fragment, container, false);
        setHasOptionsMenu(true);
        mList = (ListView)v.findViewById(R.id.cities);
        mList.setDivider(null);
        View footerView = inflater.inflate(R.layout.blank_footer_view, mList, false);
        mList.addFooterView(footerView);
        mAdapter = new WorldClockAdapter(fa);
        mList.setAdapter(mAdapter);
        mList.setOnTouchListener(new OnTouchListener() {
            private final float MAX_MOVEMENT_ALLOWED = 20;
            private float mLastTouchX, mLastTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case (MotionEvent.ACTION_MOVE):
                        float xDiff = Math.abs(event.getX()-mLastTouchX);
                        float yDiff = Math.abs(event.getY()-mLastTouchY);
                        if (xDiff >= MAX_MOVEMENT_ALLOWED || yDiff >= MAX_MOVEMENT_ALLOWED) {
                            mHandler.removeCallbacksAndMessages(null);
                        }
                        break;
                    default:
                        mHandler.removeCallbacksAndMessages(null);
                }
                return false;
            }
        });
        mDefaultClockStyle = fa.getResources().getString(R.string.default_clock_style);
        return v;
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater mi) {
        super.onCreateOptionsMenu(menu,mi);
        menu.clear();
        mi.inflate(R.menu.cities_menu, menu);
    }
    
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
        fa.getMenuInflater().inflate(R.menu.cities_menu, menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_settings:
                startActivity(new Intent(fa, SettingsActivity.class));
                return true;
            case R.id.cities_button:
                startActivity(new Intent(fa, CitiesActivity.class));
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume () {
        super.onResume();
        mPrefs.registerOnSharedPreferenceChangeListener(this);

        mQuarterlyIntent = Utils.startAlarmOnQuarterHour(fa);
        // Besides monitoring when quarter-hour changes, monitor other actions that
        // effect clock time
        IntentFilter filter = new IntentFilter(Utils.ACTION_ON_QUARTER_HOUR);
        filter.addAction(Alarms.ALARM_DONE_ACTION);
        filter.addAction(Alarms.ALARM_SNOOZE_CANCELLED);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        filter.addAction(Intent.ACTION_LOCALE_CHANGED);
        fa.registerReceiver(mIntentReceiver, filter);

        // Resume can invoked after changing the cities list or a change in locale
        if (mAdapter != null) {
            mAdapter.loadCitiesDb(fa);
            mAdapter.reloadData(fa);
        }
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.cancelAlarmOnQuarterHour(fa, mQuarterlyIntent);
        fa.unregisterReceiver(mIntentReceiver);
    }

    @Override
    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(SettingsActivity.KEY_CLOCK_STYLE)) {
            fa.recreate();
        }
    }
 }

/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.alarmclock;

import com.lennox.deskclock.AlarmClock;
import com.lennox.deskclock.R;
import com.lennox.deskclock.SettingsActivity;
import com.lennox.deskclock.utils.ThemeUtils;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * Simple widget to show analog clock.
 */
public class AnalogAppWidgetProvider extends AppWidgetProvider {
    private static final boolean DEBUG = false;

    static final String TAG = "AnalogAppWidgetProvider";

    static String[] clockStyleDrawables;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        if (DEBUG) Log.d(TAG, "onUpdate");

        final int N = appWidgetIds.length;

        StringBuilder clockDials = new StringBuilder("appwidget_normal_clock_dial");
        String thisPackageName = context.getPackageName();
        if (!ThemeUtils.getThemePackageName(context,thisPackageName).equals(thisPackageName)) {
            clockDials.append("|appwidget_theme_clock_dial");
        }

        clockStyleDrawables = clockDials.toString().split("\\|");

        for (int i=0; i<N; i++) {
            int selectedPos = AnalogAppWidgetConfigure.loadClockPref(context, appWidgetIds[i]);

            String resName = clockStyleDrawables[selectedPos];

            updateAppWidget(context, appWidgetManager, appWidgetIds[i], resName);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        if (DEBUG) Log.d(TAG, "onDeleted");

        final int N = appWidgetIds.length;
        for (int i=0; i<N; i++) {
            AnalogAppWidgetConfigure.deleteClockPref(context, appWidgetIds[i]);
        }
    }

    @Override
    public void onEnabled(Context context) {
        if (DEBUG) Log.d(TAG, "onEnabled");
    }

    @Override
    public void onDisabled(Context context) {
        if (DEBUG) Log.d(TAG, "onDisabled");
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
            int appWidgetId, String resName) {
        if (DEBUG) Log.d(TAG, "updateAppWidget appWidgetId=" + appWidgetId);

        RemoteViews views = new RemoteViews(context.getPackageName(),
            R.layout.analog_appwidget);

        views.setOnClickPendingIntent(R.id.analog_appwidget,
                PendingIntent.getActivity(context, 0,
                    new Intent(context, AlarmClock.class), 0));

        Drawable clockDial = ThemeUtils.getDrawable(context, resName);
        Bitmap clockDialBitmap = ((BitmapDrawable)clockDial).getBitmap();
        views.setImageViewBitmap(R.id.imageViewClockDial, clockDialBitmap);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }


}


/*
 * Copyright (C) 2012 The CyanogenMod Project (DvTonder)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.alarmclock;

import android.app.AlarmManager;
import android.app.Service;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;

import java.util.Date;
import java.util.Locale;

import com.lennox.deskclock.R;
import com.lennox.deskclock.SettingsActivity;

public class ClockWidgetService extends Service {
    private static final String TAG = "ClockWidgetService";
    private static boolean D = false;

    public static final String ACTION_REFRESH = "com.lennox.deskclock.action.REFRESH_WIDGET";

    private static Typeface mRobotoSlab;

    private int[] mWidgetIds;
    private AppWidgetManager mAppWidgetManager;

    private BroadcastReceiver bReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getApplicationContext().unregisterReceiver(bReceiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ComponentName thisWidget = new ComponentName(this, ClockWidgetProvider.class);
        mAppWidgetManager = AppWidgetManager.getInstance(this);
        mWidgetIds = mAppWidgetManager.getAppWidgetIds(thisWidget);

        if ( mRobotoSlab == null ) {
            mRobotoSlab =
                Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/RobotoSlab.ttf");
        }

        bReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                handleReceivedCommand(intent);
            }
        };

        handleReceivedCommand(intent);

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(ACTION_REFRESH);
        getApplicationContext().registerReceiver(bReceiver, intentFilter);

        return START_STICKY;
    }

    private void handleReceivedCommand(Intent intent) {
        ComponentName thisWidget = new ComponentName(this, ClockWidgetProvider.class);
        mAppWidgetManager = AppWidgetManager.getInstance(this);
        mWidgetIds = mAppWidgetManager.getAppWidgetIds(thisWidget);

        if (mWidgetIds != null && mWidgetIds.length != 0) {
            refreshWidget();
        }
    }

    /**
     * Reload the widget including the Alarm, and clock
     */
    private void refreshWidget() {
        // Get things ready
        RemoteViews remoteViews;

        // Update the widgets
        for (int id : mWidgetIds) {

            remoteViews = new RemoteViews(getPackageName(), R.layout.digital_appwidget);

            // Always Refresh the Clock widget
            refreshClock(remoteViews);
            refreshAlarmStatus(remoteViews);

            Context ctx = getBaseContext();

            float ratio = WidgetUtils.getScaleRatio(this, id);
            setClockSize(remoteViews, ratio);

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
            boolean showAmPm = sharedPref.getBoolean(SettingsActivity.KEY_DIGITAL_WIDGET_SHOW_AMPM, false);

            boolean b24 = DateFormat.is24HourFormat(ctx);
            int resIdHour, resIdMinute;

            if (b24) {
                resIdHour = R.string.widget_24_hours_format_h;
                resIdMinute = R.string.widget_24_hours_format_m;
            } else if (showAmPm) {
                resIdHour = R.string.widget_12_hours_format_h;
                resIdMinute = R.string.widget_12_hours_format_m;
            } else {
                resIdHour = R.string.widget_12_hours_format_h;
                resIdMinute = R.string.widget_24_hours_format_m;
            }
            String formatHour = ctx.getString(resIdHour);
            String formatMinute = ctx.getString(resIdMinute);

            Date updateTime = new Date();
            String hourString = DateFormat.format(formatHour, updateTime).toString();
            String minuteString = DateFormat.format(formatMinute, updateTime).toString();

            remoteViews.setTextViewText(R.id.the_clock1, hourString);
            remoteViews.setTextViewText(R.id.the_clock2, minuteString);

            String formatDate = ctx.getString(R.string.abbrev_wday_month_day_no_year);
            String dateString = DateFormat.format(formatDate, updateTime).toString();

            remoteViews.setTextViewText(R.id.date, dateString);

            // Do the update
            mAppWidgetManager.updateAppWidget(id, remoteViews);
        }
    }

    //===============================================================================================
    // Clock related functionality
    //===============================================================================================
    private void refreshClock(RemoteViews clockViews) {

        // Date/Alarm is common to both clocks, set it's size
        refreshDateAlarmFont(clockViews);

        // Register an onClickListener on Clock, starting DeskClock
        Intent i = WidgetUtils.getDefaultClockIntent(this);
        if (i != null) {
            PendingIntent pi = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            clockViews.setOnClickPendingIntent(R.id.digital_appwidget, pi);
        }
    }

    private void refreshDateAlarmFont(RemoteViews clockViews) {
        // Show the panel
        clockViews.setViewVisibility(R.id.nextAlarm, View.VISIBLE);
    }

    private void setClockSize(RemoteViews clockViews, float scale) {
        float fontSize = getResources().getDimension(R.dimen.widget_big_font_size);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            clockViews.setTextViewTextSize(R.id.the_clock1, TypedValue.COMPLEX_UNIT_PX, fontSize * scale);
            clockViews.setTextViewTextSize(R.id.the_clock2, TypedValue.COMPLEX_UNIT_PX, fontSize * scale);
        }
    }

    //===============================================================================================
    // Alarm related functionality
    //===============================================================================================
    private void refreshAlarmStatus(RemoteViews alarmViews) {
        String nextAlarm = getNextAlarm();
        if (!TextUtils.isEmpty(nextAlarm)) {
            alarmViews.setTextViewText(R.id.nextAlarm, nextAlarm.toString().toUpperCase(Locale.getDefault()));
            return;
        }
        // No alarm set or Alarm display is hidden, hide the views
        alarmViews.setViewVisibility(R.id.nextAlarm, View.GONE);
    }

    /**
     * @return A formatted string of the next alarm or null if there is no next alarm.
     */
    private String getNextAlarm() {
        String nextAlarm = Settings.System.getString(
                getContentResolver(), Settings.System.NEXT_ALARM_FORMATTED);
        if (nextAlarm == null || TextUtils.isEmpty(nextAlarm)) {
            return null;
        }
        return nextAlarm;
    }

}
